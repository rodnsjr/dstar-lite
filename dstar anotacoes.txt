x: 0 y: 1
x: 1 y: 2
x: 2 y: 3
x: 3 y: 2
x: 3 y: 1

O meu:
X 0 Y 1
X 1 Y 0
X 2 Y -1
X 3 Y -1
X 4 Y 0
X 3 Y 1

Parece que o m?todo updateStart n?o sincroniza com a lista ou algo assim:
No ultimo caso:
Path - Obscatulos Atualizados - Segunda Sala
X 5 Y 4
X 4 Y 2
X 5 Y 3
X 6 Y 4
X 7 Y 5
X 7 Y 6
X 7 Y 7
X 8 Y 8
Ele volta a dois estados anteriores

E o updateGoal tamb?m est? quebrado!
Tem que revisar ambos os m?todos para conseguir gerar uma simula??o legal para o terceiro teste!

1. Os casos de uso, um deles ? uma navega??o comum para mostrar o algoritmo.
2. Outro ser? seguindo a ideia de modelar um mapa, e ao longo da trajet?ria ir recalculando com a simula??o da detec??o de obst?culos.
3. O terceiro ? o de ir desenhando o mapa conforme o usu?rio faz a trajet?ria, e depois ao voltar para o ponto de partida o usu?rio
    faz a melhor trajet?ria baseado no mapa desenhado utilizando o D*.

O D* Lite ? baseado na extens?o do LPA*

LPA* ? uma vers?o incremental do A*, ele faz busca finita em grafos, o qual o custo das arestas aumentam ou diminuem ao longo do tempo.
Pra isso ele tem os predecessores e os sucessores de cada vertice s.
Os predecessores de s s?o para os casos de que S.0 < c(s,s')
? usado o g(s) para a dist?ncia de cada vertice, e o valor h para os valores de her?stica. 

variaveis
g -> msm q o A*
rhs -> ? um valor a frente do pr?xmo passo, baseado nos valores de g.
ele sempre satisfaz a rela??o: 
{0 se s=start ou min(g(s')+(cs',s)}

Um vertice ? localmente consistente se os valures de g equivalem aos valores de rhs. Se e somente se todos os vertices forem localmente consistentes ent?o os valores de g de todos os vertices s?o equivalentes a suas dist?ncias respectivas
Nesse caso pode-se tra?ar o caminho mais curto do vertice de in?cio a qualquer outro vertice 'u' sempre fazendo a transi??o do vertice atual 's', come?ando em 'u' para qualquer predecessor de s' que minimize g(s') + c(s', s), at? o s inicial for alcan?ado.

O terreno para o D* Lite ? modelado como um grafo de 8 conex?es, o custo das arestas ? inicialmente 1, eles podem ser mudados para infinito caso o rob? descubra que elas n?o podem ser atravessadas.

O desafio ? entender como que o mapa ? visualizado pelo algoritmo, e desenhar uma simula??o dele no python. Ele ? dividido em celulas com resolu??o uniforme, na implementa??o ela seria a CellInfo e est? na CellHash. O m?todo updateCell atualiza as celulas do 'mapa', lembrando que os custos INFINITY n?o s?o percorr?veis pelo robo.


Consumindo o Servi?o do D *
curl -X GET -H "Content-type: application/json" -H "Accept: application/json"  "http://localhost:800/route/" -d '{"start": { "x" : 0, "y" : 1 }, "goal" : { "x" : 3, "y" : 1 } }'


00BBB
010B?
00BBB

Comandos b?sicos de vis?o:
w -> ver a frente

Comandos de movimenta??o:
0 -> andar conforme o d*
1 -> reatualizar e replanejar conforme vis?o


check #1. Readme.md explicando os c?digos
#2. Report, come?ar a organizar as se??es
#3. Montar um plano de como fazer o projeto funcionar numa simula??o do todo


1. Introdu??o
2. Escopo
3. Descri??o do Problema
4. Solu??o Proposta
5. Conclus?es


#Simula??o
Parte 1.
Tentar deixar em tempo real, a leitura dos pontos, e replanejamento da rota

Parte 2.
Corrigir os bugs que est?o acontecendo, pra poder fazer um planejamento de ida, atualiza??o e volta.
Seguindo a mesma ideia da primeira simula??o

Resultados:
Ter um planejamento bacana para os resultados do algoritmo em si.