"""A basic (single function) API written using hug"""
import hug
from dstar import DStarLite
from map import Point

@hug.get('/route')
def route(startX, startY, goalX, goalY):
    """Gera uma rota de navegação para o usuário"""
    dstar = DStarLite()
    startPoint = Point(int(startX), int(startY))
    goalPoint = Point(int(goalX), int(goalY))
    dstar.initialize(startPoint, goalPoint)
    dstar.replan()
    exit = ""
    for index, value in enumerate(dstar.path):
        exit += "Index %01d X %01d - Y %01d " % (index, value.point.x, value.point.y)
    return exit

print(hug.test.get(route, 'route', { 'start' : { 'x' : 0, 'y' : 1}, 'goal' : { 'x' : 3, 'y' : 1} }))