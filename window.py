import sys
from map import Point
from dstar import DStarLite
from test import TestMap
from PyQt4 import QtGui, QtCore


class Example(QtGui.QWidget):
    def __init__(self):
        super(Example, self).__init__()
        self.initUI()
        self.currentIndex = -1
        self.obstacles = set()

    def initUI(self):

        self.grid = QtGui.QGridLayout()
        self.setLayout(self.grid)

        self.testMap = TestMap.mapwithrooms()

        self.start = Point(1, 1)
        self.goal = Point(8, 8)

        for obstacle in self.testMap.map:
            button = QtGui.QPushButton('O')
            # Adiciona o obstaculo em linha/coluna
            self.grid.addWidget(button, obstacle.y, obstacle.x)

        start = QtGui.QPushButton('S')
        goal = QtGui.QPushButton('G')
        self.grid.addWidget(start, self.start.y, self.start.x)
        self.grid.addWidget(goal, self.goal.y, self.goal.x)

        self.move(300, 150)
        self.setWindowTitle('D * Lite')
        self.show()

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_0:
            self.startDStar()
        elif e.key() == QtCore.Qt.Key_1:
            self.updateMap()
        elif e.key() == QtCore.Qt.Key_W:
            self.seeAhead(e.key())
        elif e.key() == QtCore.Qt.Key_S:
            self.seeAhead(e.key())
        elif e.key() == QtCore.Qt.Key_Escape:
            self.close()

    def startDStar(self):
        # Start the dstar
        if self.currentIndex == -1:
            self.dstar = DStarLite()
            self.dstar.initialize(self.start, self.goal)
            self.dstar.replan()
            self.currentIndex += 1
        else:
            self.drawPath()

    def drawPath(self):
        self.currentIndex += 1
        currentButton = QtGui.QPushButton('S%01d' % self.currentIndex)
        currentPoint = self.dstar.path[self.currentIndex].point
        self.grid.addWidget(currentButton, currentPoint.y, currentPoint.x)

    def updateMap(self):
        if len(self.obstacles) > 0:
            current = self.dstar.path[self.currentIndex].point
            self.dstar.updateStart(current)
            for value in self.obstacles:
                self.dstar.updateCell(value, -1)
            self.dstar.replan()
            self.currentIndex = 0

    def seeAhead(self, pos):

        def openFront(obstacles):
            sight = 3
            while sight > 0:
                item1 = self.grid.itemAtPosition(current.y - 1, current.x + sight)
                item2 = self.grid.itemAtPosition(current.y, current.x + sight)
                item3 = self.grid.itemAtPosition(current.y + 1, current.x + sight)

                if item1 is not None:
                    item1.widget().setStyleSheet("background-color: red")
                    obstacles.add(Point(current.x + sight, current.y - 1))
                if item2 is not None:
                    item2.widget().setStyleSheet("background-color: red")
                    obstacles.add(Point(current.x + sight, current.y))
                if item3 is not None:
                    item3.widget().setStyleSheet("background-color: red")
                    obstacles.add(Point(current.x + sight, current.y + 1))

                sight -= 1

        def openBelow(obstacles):
            sight = 3
            while sight > 0:
                item1 = self.grid.itemAtPosition(current.y + sight, current.x - 1)
                item2 = self.grid.itemAtPosition(current.y + sight, current.x)
                item3 = self.grid.itemAtPosition(current.y + sight, current.x + 1)

                if item1 is not None:
                    item1.widget().setStyleSheet("background-color: red")
                    point = Point(current.x - 1, current.y + sight)
                    obstacles.add(point)
                if item2 is not None:
                    item2.widget().setStyleSheet("background-color: red")
                    point = Point(current.x, current.y + sight)
                    obstacles.add(point)
                if item3 is not None:
                    item3.widget().setStyleSheet("background-color: red")
                    point = Point(current.x + 1, current.y + sight)
                    obstacles.add(point)

                sight -= 1

        if self.currentIndex > -1:
            current = self.dstar.path[self.currentIndex].point

            if pos == QtCore.Qt.Key_W:
                openFront(self.obstacles)
            else:
                openBelow(self.obstacles)

def main():
    app = QtGui.QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()