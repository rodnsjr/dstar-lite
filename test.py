from map import Point
from dstar import DStarLite

class TestMap():
    def __init__(self, map):
        self.map = map

    def obstacles(self, dstar):
        for value in self.map:
            dstar.updateCell(value, -1)

    @staticmethod
    def walls(init, size):
        # Build a the scenario based on a map
        # update the walls by the sides
        # Draw the outside walls
        start = init
        end = size
        walls = set()
        while start <= size and end >= init:
            walls.add(Point(start, init))
            walls.add(Point(init, end))
            walls.add(Point(size, start))
            walls.add(Point(start, size))
            start += 1
            end -= 1
        return TestMap(walls)

    @staticmethod
    def room1(testmap):
        # Monta a primeira sala
        testmap.map.add(Point(4, 1))
        testmap.map.add(Point(4, 3))
        testmap.map.add(Point(4, 4))
        testmap.map.add(Point(4, 5))
        testmap.map.add(Point(4, 6))
        testmap.map.add(Point(4, 7))
        testmap.map.add(Point(1, 4))
        testmap.map.add(Point(2, 4))
        testmap.map.add(Point(3, 4))

    @staticmethod
    def room2(testmap):
        # Monta a segunda sala
        testmap.map.add(Point(6, 1))
        testmap.map.add(Point(6, 3))
        testmap.map.add(Point(6, 4))
        testmap.map.add(Point(7, 4))
        testmap.map.add(Point(8, 4))

    @staticmethod
    def room3(testmap):
        # E monta a ultima sala
        testmap.map.add(Point(6, 5))
        testmap.map.add(Point(6, 6))
        testmap.map.add(Point(6, 7))

    @staticmethod
    def mapwithrooms():
        testmap = TestMap.walls(0, 9)

        TestMap.room1(testmap)
        TestMap.room2(testmap)
        TestMap.room3(testmap)

        return testmap

def simpleTest():
    dstar = DStarLite()

    startPoint = Point(0, 1)
    goalPoint = Point(3, 1)

    dstar.initialize(startPoint, goalPoint)

    dstar.updateCell(Point(2, 1), -1)
    dstar.updateCell(Point(2, 0), -1)
    dstar.updateCell(Point(2, 2), -1)
    dstar.updateCell(Point(3, 0), -1)

    dstar.replan()

    return dstar.path

def mediumTest():
    dstar = DStarLite()
    startPoint = Point(1, 1)
    goalPoint = Point(8, 8)
    dstar.initialize(startPoint, goalPoint)

    testMap = TestMap.walls(0, 9)

    for item in testMap.map:
        dstar.updateCell(item, -1)

    dstar.replan()

    print_path(dstar.path, "Primeiro caminho")

    dstar.updateStart(Point(3, 2))

    testMap = TestMap(set())

    TestMap.room1(testMap)

    for item in testMap.map:
        dstar.updateCell(item, -1)

    dstar.replan()
    print_path(dstar.path, "Obstaculos atualizados")

    dstar.updateStart(Point(5, 4))

    # Monta a segunda sala
    testMap = TestMap(set())
    TestMap.room2(testMap)

    # E monta a ultima sala
    TestMap.room3(testMap)

    for item in testMap.map:
        dstar.updateCell(item, -1)

    dstar.replan()

    print_path(dstar.path, "Obscatulos Atualizados - Segunda Sala")

    return dstar.path

def complexTest():
    dstar = DStarLite()
    startPoint = Point(1, 1)
    goalPoint = Point(8, 8)
    dstar.initialize(startPoint, goalPoint)

    # Build a the scenario based on a map
    # update the walls by the sides
    # Draw the outside walls
    start = 0
    end = 9
    walls = set()
    while start < 9 and end > 0:
        walls.add(Point(start, 0))
        walls.add(Point(0, end))
        walls.add(Point(9, start))
        walls.add(Point(start, 9))
        start += 1
        end -= 1

    for item in walls:
        dstar.updateCell(item, -1)

    dstar.replan()

    print_path(dstar.path, "Primeiro caminho")

    # Monta a primeira sala
    walls.add(Point(4, 1))
    walls.add(Point(4, 3))
    walls.add(Point(4, 4))
    walls.add(Point(4, 5))
    walls.add(Point(4, 6))
    walls.add(Point(4, 7))
    walls.add(Point(1, 4))
    walls.add(Point(2, 4))
    walls.add(Point(3, 4))

    # Monta a segunda sala
    walls.add(Point(6, 1))
    walls.add(Point(6, 3))
    walls.add(Point(6, 4))
    walls.add(Point(7, 4))
    walls.add(Point(8, 4))

    # E monta a ultima sala
    walls.add(Point(6, 5))
    walls.add(Point(6, 6))
    walls.add(Point(6, 7))

    for item in walls:
        dstar.updateCell(item, -1)

    dstar.replan()
    print_path(dstar.path, "O Caminho de Volta")

    return dstar.path

def randomHardTest():
    dstar = DStarLite()
    startPoint = Point(0, 1)
    goalPoint = Point(3, 1)
    dstar.initialize(startPoint, goalPoint)

    dstar.updateCell(Point(2, 1), -1)
    dstar.updateCell(Point(2, 0), -1)
    dstar.updateCell(Point(2, 2), -1)
    dstar.updateCell(Point(3, 0), -1)

    dstar.replan()
    dstar.updateGoal(Point(3, 2))

    return dstar.path

def print_path(path, name):
    print("Path - %s" % name)
    for value in path:
        print("X %01d Y %01d" % (value.point.x, value.point.y))

mediumTest()