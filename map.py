import functools

class Pair(object):
    def __init__(self, value1=0, value2=0):
        self.value1 = value1
        self.value2 = value2

    def __hash__(self):
        hashCode = self.value1.__hash__ if self.value1 is not None else 0
        hashCode += self.value2.__hash__ if self.value2 is not None else 0
        return hashCode

    def __eq__(self, other):
        if self.value1 is None and self.value2 is None:
            return other.value1 is None and other.value2 is None
        if other.value1 is None and other.value2 is None:
            return False
        return True

class CellInfo(object):
    def __init__(self, g, rhs, cost):
        self.g = g
        self.rhs = rhs
        self.cost = cost

class Point(object):
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

@functools.total_ordering
class State(object):
    def __init__(self, point=Point(0, 0), k=Pair(0, 0)):
        self.point = point
        self.k = k

    def equals(self, state):
        return self.point.x == state.point.x and self.point.y == state.point.y

    def notEquals(self, state):
        return self.point.x != state.point.x or self.point.y != state.point.y

    def greaterThan(self, state):
        if self.k.value1 - 0.000001 > state.k.value1:
            return True
        elif self.k.value1 < state.k.value1 - 0.000001:
            return False
        return self.k.value2 > state.k.value2

    def lessThan(self, state):
        if self.k.value1 + 0.000001 < state.k.value1:
            return True
        elif self.k.value1 - 0.000001 > state.k.value1:
            return False
        return self.k.value2 < state.k.value2

    def lessThenOrEqualTo(self, state):
        if self.k.value1 < state.k.value1:
            return True
        elif self.k.value1 > state.k.value1:
            return False
        return self.k.value2 < state.k.value2 + 0.000001

    def __hash__(self, *args, **kwargs):
        return self.point.x + 34245 * self.point.y

    def __eq__(self, other):
        return self.point.x == other.point.x and self.point.y == other.point.y

    def __lt__(self, other):
        return ((self.k.value1, self.k.value2) <
                (other.k.value1, other.k.value2))

    def __gt__(self, other):
        return ((self.k.value1, self.k.value2) >
                (other.k.value1, other.k.value2))

    def __str__(self):
        return "X %01d, Y %01d" % self.point.x + self.point.y

    @staticmethod
    def create(state):
        return State(Point(state.point.x, state.point.y), Pair(state.k.value1, state.k.value2))

