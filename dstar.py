from map import Pair, Point, State, CellInfo
from queue import PriorityQueue
import sys
import math

M2_SQRT = math.sqrt(2)


def eightCondist(p1, p2):
    min = math.fabs(p1.x - p2.x)
    max = math.fabs(p1.y - p2.y)
    if (min > max):
        tmp = min
        min = max
        max = tmp
    return (M2_SQRT - 1.0) * min + max


def euclidianDist(pointA, pointB):
    x = pointA.x - pointB.x
    y = pointA.y - pointB.y
    return math.sqrt(x * x + y * y)

class DStarLite(object):
    MAX_STEPS = 8000

    def __init__(self):
        self.startState = State(Point(0, 0), Pair(0, 0))
        self.goalState = State(Point(0, 0), Pair(0, 0))
        self.lastState = State(Point(0, 0), Pair(0, 0))
        self.path = []
        self.openQueue = PriorityQueue()
        "<State, CellInfo>"
        self.cellHash = dict()
        "<State,Float>"
        self.openHash = dict()
        self.k_m = 0

    def initialize(self, startPoint, goalPoint):
        self.cellHash.clear()
        self.openHash.clear()
        self.path.clear()
        self.openQueue.queue.clear()
        self.startState.point = startPoint
        self.goalState.point = goalPoint

        tmp = CellInfo(0, 0, 1)
        self.cellHash[self.goalState] = tmp

        tmp = CellInfo(0, 0, 1)
        tmp.rhs = self.heuristic(self.startState, self.goalState)
        tmp.g = tmp.rhs
        self.cellHash[self.startState] = tmp
        self.startState = self.calculateKey(self.startState)
        self.lastState = self.startState

    def calculateKey(self, state):
        val = min(self.getRhs(state), self.getG(state))

        state.k.value1 = val + self.heuristic(state, self.startState) + self.k_m
        state.k.value2 = val

        return state

    def updateVertex(self, state):
        if state.notEquals(self.goalState):
            sucessors = self.getSuccessors(state)
            tmp = sys.float_info.max
            tmp2 = 0

            for i in sucessors:
                tmp2 = self.getG(i) + self.cost(state, i)
                if tmp2 < tmp:
                    tmp = tmp2

            if not self.close(self.getRhs(state), tmp):
                self.setRhs(state, tmp)

        if not self.close(self.getG(state), self.getRhs(state)):
            self.insert(state)

    def computeShortestPath(self):
        def whileCheck():
            emptyQueue = self.openQueue.empty()
            self.startState = self.calculateKey(self.startState)
            isFirstInLineLess = self.openQueue.queue[0].lessThan(self.startState)
            stateValues = self.getRhs(self.startState) != self.getG(self.startState)
            return (not emptyQueue) and (isFirstInLineLess or stateValues)

        s = []

        if self.openQueue.empty():
            return 1

        k = 0

        while whileCheck():
            if (k + 1) > self.MAX_STEPS:
                return -1
            test = (self.getRhs(self.startState) != self.getG(self.startState))

            k += 1

            u = 0
            while True:
                if self.openQueue.empty():
                    return 1

                u = self.openQueue.get()

                if not self.isOnOpenList(u):
                    continue
                if (not u.lessThan(self.startState)) and (not test):
                    return 2
                break

            del self.openHash[u]

            oldState = State.create(u)

            if oldState.lessThan(self.calculateKey(u)):
                self.insert(u)
            elif self.getG(u) > self.getRhs(u):
                self.setG(u, self.getRhs(u))
                predecessors = self.getPredecessors(u)
                for value in predecessors:
                    self.updateVertex(value)
            else:
                # // g <= rhs, state has got worse
                self.setG(u, sys.float_info.max)
                precedessors = self.getPredecessors(u)

                for value in precedessors:
                    self.updateVertex(value)

                self.updateVertex(u)

        return 0


    def getRhs(self, state):
        if state == self.goalState:
            return 0
        if state not in self.cellHash:
            return self.heuristic(state, self.goalState)
        return self.cellHash[state].rhs

    def getG(self, state):
        if not state in self.cellHash:
            return self.heuristic(state, self.goalState)
        return self.cellHash[state].g

    def heuristic(self, stateA, stateB):
        return eightCondist(stateA.point, stateB.point) * 1

    def replan(self):
        '''
            Equivalente a funcao main do D *
            Replaneja o caminho ideal para
            os casos de movimento do agente.
        '''
        self.path.clear()

        res = self.computeShortestPath()

        if res < 0:
            return False

        n = []

        cur = self.startState

        # max float
        if self.getG(self.startState) == sys.float_info.max:
            return False

        while cur.notEquals(self.goalState):
            self.path.append(cur)
            n = []
            n = self.getSuccessors(cur)

            if not n:
                return False

            cmin = sys.float_info.max
            tmin = 0

            stateMin = State(Point(0, 0), Pair(0, 0))

            for state in n:
                val = self.cost(cur, state)
                val2 = euclidianDist(state.point, self.goalState.point) + euclidianDist(self.startState.point, state.point)
                val += self.getG(state)

                if self.close(val, cmin):
                    if tmin > val2:
                        tmin = val2
                        cmin = val
                        stateMin = state
                elif val < cmin:
                    tmin = val2
                    cmin = val
                    stateMin = state


            n = []
            cur = State.create(stateMin)

        self.path.append(self.goalState)

        return True

    def getSuccessors(self, state):
        s = []

        if self.occupied(state):
            return s

        # Generate the successors, starting at the immediate right,
        # Moving in a clockwise manner
        tempState = State(Point(state.point.x + 1, state.point.y), Pair(-1.0, -1.0))
        s.insert(0, tempState)
        tempState = State(Point(state.point.x + 1, state.point.y + 1), Pair(-1.0, -1.0))
        s.insert(0, tempState)
        tempState = State(Point(state.point.x, state.point.y + 1), Pair(-1.0, -1.0))
        s.insert(0, tempState)
        tempState = State(Point(state.point.x - 1, state.point.y + 1), Pair(-1.0, -1.0))
        s.insert(0, tempState)
        tempState = State(Point(state.point.x - 1, state.point.y), Pair(-1.0, -1.0))
        s.insert(0, tempState)
        tempState = State(Point(state.point.x - 1, state.point.y - 1), Pair(-1.0, -1.0))
        s.insert(0, tempState)
        tempState = State(Point(state.point.x, state.point.y - 1), Pair(-1.0, -1.0))
        s.insert(0, tempState)
        tempState = State(Point(state.point.x + 1, state.point.y - 1), Pair(-1.0, -1.0))
        s.insert(0, tempState)

        return s

    def getPredecessors(self, state):
        s = []

        tempState = State(Point(state.point.x + 1, state.point.y), Pair(-1.0, -1.0))
        if not self.occupied(tempState):
            s.insert(0, tempState)
        tempState = State(Point(state.point.x + 1, state.point.y + 1), Pair(-1.0, -1.0))
        if not self.occupied(tempState):
            s.insert(0, tempState)
        tempState = State(Point(state.point.x, state.point.y + 1), Pair(-1.0, -1.0))
        if not self.occupied(tempState):
            s.insert(0, tempState)
        tempState = State(Point(state.point.x - 1, state.point.y + 1), Pair(-1.0, -1.0))
        if not self.occupied(tempState):
            s.insert(0, tempState)
        tempState = State(Point(state.point.x - 1, state.point.y), Pair(-1.0, -1.0))
        if not self.occupied(tempState):
            s.insert(0, tempState)
        tempState = State(Point(state.point.x - 1, state.point.y - 1), Pair(-1.0, -1.0))
        if not self.occupied(tempState):
            s.insert(0, tempState)
        tempState = State(Point(state.point.x, state.point.y - 1), Pair(-1.0, -1.0))
        if not self.occupied(tempState):
            s.insert(0, tempState)
        tempState = State(Point(state.point.x + 1, state.point.y - 1), Pair(-1.0, -1.0))
        if not self.occupied(tempState):
            s.insert(0, tempState)
        return s

    def updateStart(self, point):
        # state = State(point, Pair(self.startState.k.value1, self.startState.k.value2))
        self.startState.point.x = point.x
        self.startState.point.y = point.y
        self.k_m += self.heuristic(self.lastState, self.startState)
        self.startState = self.calculateKey(self.startState)
        self.lastState = self.startState

    def updateGoal(self, point):
        '''
            This is somewhat of a hack, to change the position of
            the goal we first save all of the non - empty nodes on
            the map, clear the map, move the goal and add re - add
            all of the non - empty cells. Since most of these cells
            are not between the start and goal this does not seem to
            hurt performance too much.Also, it frees up a good deal
            of memory we are probably not going to use.
        '''
        # Pair<IPoint2, Double>
        toAdd = []
        for key, value in self.cellHash.items():
            if not self.close(value.cost, 1):
                tempPoint = Pair(Point(key.point.x, key.point.y), value.cost)
                toAdd.append(tempPoint)
        self.cellHash.clear()
        self.openHash.clear()
        self.openQueue.queue.clear()
        self.k_m = 0

        self.goalState.point.x = point.x
        self.goalState.point.y = point.y

        tmp = CellInfo(0, 0, 1)
        self.cellHash[self.startState] = tmp
        self.startState = self.calculateKey(self.startState)

        self.lastState = self.startState

        for item in toAdd:
            self.updateCell(item.value1, item.value2)

    def isOnOpenList(self, state):
        if not state in self.openHash:
            return False
        if not self.close(self.keyHashCode(state), self.openHash[state]):
            return False
        return True

    def setG(self, state, g):
        self.makeNewCell(state)
        self.cellHash[state].g = g

    def setRhs(self, state, rhs):
        self.makeNewCell(state)
        self.cellHash[state].rhs = rhs

    def makeNewCell(self, state):
        if not state in self.cellHash:
            tmp = CellInfo(0, 0, 0)
            tmp.rhs = self.heuristic(state, self.goalState)
            tmp.g = tmp.rhs
            tmp.cost = 1
            self.cellHash[state] = tmp

    def updateCell(self, point, value):
        tmpState = State(point, Pair(0, 0))

        if not tmpState.equals(self.startState) or tmpState.equals(self.goalState):
            self.makeNewCell(tmpState)
            self.cellHash[tmpState].cost = value
            self.updateVertex(tmpState)

    def insert(self, state):
        state = self.calculateKey(state)
        csum = self.keyHashCode(state)
        self.openHash[state] = csum
        self.openQueue.put(state)

    def keyHashCode(self, state):
        return state.k.value1 + 1193 * state.k.value2

    def occupied(self, state):
        if not state in self.cellHash:
            return False
        return self.cellHash[state].cost < 0

    def cost(self, stateA, stateB):
        xd = math.fabs(stateA.point.x - stateB.point.x)
        yx = math.fabs(stateA.point.y - stateB.point.y)
        scale = 1
        if xd + yx > 1:
            scale = M2_SQRT
        if not stateA in self.cellHash:
            return scale * 1
        return scale * self.cellHash[stateA].cost

    def close(self, x, y):
        if x == sys.float_info.max and y == sys.float_info.max:
            return True
        return math.fabs(x - y) < 0.00001

    def path(self):
        return self.path
